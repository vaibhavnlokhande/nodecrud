const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/Edureka", { useNewUrlParser: true , useUnifiedTopology: true } ,(error)=>{
    if(!error){
        console.log("succcess connected");

    }
    else{
        console.log("error connecting to db");
    }
});

const Course = require("./course.model");