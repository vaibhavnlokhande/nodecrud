

const connection = require("./model");

const express = require('express');
var app = express();
const path = require('path');
const exphb = require('express-handlebars');
const bodyparser = require('body-parser');

const CourseController = require("./controllers/courses");

app.set('views', path.join(__dirname, '/views/'));

app.engine('hbs', exphb({
     extname: 'hbs',
      defaultLayout: 'mainLayout', 
      layoutDir: __dirname + '/views/layouts/'
     }));

app.set('view engine', 'hbs');

app.use(bodyparser.urlencoded({
    extended: true
    }));

    app.get('/', (req, res) => {
        //res.send('<h1>Hello world</h1>')
         res.render("index",{})
    })


    app.use("/course", CourseController)

    app.listen(3000, () =>{
         console.log('server started');
});